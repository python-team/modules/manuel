manuel (1.13.0-2) unstable; urgency=medium

  * Team upload.
  * Remove python3-six from Build-Depends: & autopkgtest dependencies

 -- Alexandre Detiste <tchet@debian.org>  Sun, 12 Jan 2025 15:49:26 +0100

manuel (1.13.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.

 -- Colin Watson <cjwatson@debian.org>  Tue, 12 Nov 2024 13:53:55 +0000

manuel (1.12.4-4) unstable; urgency=medium

  * Team upload.
  * Do not generate a dependency on python3-pkg-resources (Closes: #1083474)
  * Fix SyntaxWarning (Closes: #1085684)

 -- Alexandre Detiste <tchet@debian.org>  Mon, 21 Oct 2024 17:37:51 +0200

manuel (1.12.4-3) unstable; urgency=medium

  * Team upload.
  * Fix build following the removal of "setup.py test" (closes: #1080116).
  * Don't rely on "setup.py test" in autopkgtest (closes: #1079748).
  * Use pybuild-plugin-pyproject.

 -- Colin Watson <cjwatson@debian.org>  Tue, 03 Sep 2024 13:45:03 +0100

manuel (1.12.4-2) unstable; urgency=medium

  * Handle test output change on python3.11 (Closes: #1024956)

 -- James Valleroy <jvalleroy@mailbox.org>  Fri, 02 Dec 2022 15:15:39 -0500

manuel (1.12.4-1) unstable; urgency=medium

  [ Carl Suster ]
  * d/watch: switch to version 4 and track tags rather than the empty releases.

  [ James Valleroy ]
  * Test only supported python versions
  * Set Standards-Version to 4.6.1
  * Add my copyright years
  * New upstream version 1.12.4
  * Build-depend on python3-myst-parser
  * Build-depend on python3-sphinx-copybutton
  * Drop removed file from d/copyright
  * Update upstream copyright years
  * gbp: Enable pristine-tar and sign-tags

 -- James Valleroy <jvalleroy@mailbox.org>  Fri, 07 Oct 2022 09:29:43 -0400

manuel (1.10.1-4) unstable; urgency=medium

  [ Debian Janitor ]
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.6.0, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sat, 28 May 2022 01:12:28 +0100

manuel (1.10.1-3) unstable; urgency=medium

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Debian Janitor ]
  * Bump debhelper from old 11 to 12.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Update standards version to 4.5.0, no changes needed.

 -- Sandro Tosi <morph@debian.org>  Tue, 03 May 2022 23:45:28 -0400

manuel (1.10.1-2) unstable; urgency=medium

  * Team upload.

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.
  * Bump Standards-Version to 4.4.0.

  [ Andrey Rahmatullin ]
  * Drop Python 2 support.

 -- Andrey Rahmatullin <wrar@debian.org>  Wed, 07 Aug 2019 21:13:59 +0500

manuel (1.10.1-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Use 'python3 -m sphinx' instead of sphinx-build for building docs

  [ James Valleroy ]
  * d/control: Standards-Version is 4.2.1 now (no changes needed)
  * New upstream version 1.10.1
  * Drop patch 0002-Update-doctests-to-support-python3.7, applied upstream
  * d/control: Drop obsolete version restrictions for build-depends
  * d/rules: List sphinx calls in separate lines
  * d/control: Set Rules-Requires-Root to no
  * d/control: Standards-Version is 4.3.0 now (no changes needed)

 -- James Valleroy <jvalleroy@mailbox.org>  Sun, 30 Dec 2018 13:09:35 -0500

manuel (1.9.0-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Use https protocol in Format field
  * d/control: Remove ancient X-Python-Version field
  * d/control: Remove ancient X-Python3-Version field

  [ James Valleroy ]
  * Convert from git-dpm to patches unapplied format
  * d/control: Add myself to uploaders
  * Patch doctests to support python3.7 (Closes: #903524)
  * debian/compat: Bump compat level to 11
  * debian/control: Bump Standards-Version to 4.1.5
  * debian/control: Add build-depend on python3-sphinx
  * New upstream version 1.9.0
  * debian/control: Slightly tweak homepage URL
  * debian/control: Bump Standards-Version to 4.2.0

 -- James Valleroy <jvalleroy@mailbox.org>  Mon, 06 Aug 2018 19:06:24 -0400

manuel (1.8.0-5) unstable; urgency=medium

  * Team upload.

  [ Barry Warsaw ]
  * Now that dh-python and zope.testing have been fixed, the patch for
    setup.py is no longer necessary.

  [ Daniel Stender |
  * deb/control:
    + put team into Maintainer field (unconfirmed team uploads).
    + removed myself from Maintainer.
    + dropped Testsuite field (deprecated).
    + bumped standards to 3.9.8 (no changes needed).
  * deb/copyright:
    + corrected a typo.

  [ Ondřej Nový ]
  * Fixed VCS URL (https)

 -- Daniel Stender <stender@debian.org>  Thu, 30 Jun 2016 19:37:40 +0200

manuel (1.8.0-4) unstable; urgency=medium

  [ Daniel Stender ]
  * removed deb/tests/tests.

  [ Barry Warsaw ]
  * d/patches/disable-tests-requires.patch: Added to workaround an issue
    where Python will still try to download tests_requires from PyPI.

 -- Daniel Stender <debian@danielstender.com>  Tue, 03 Nov 2015 03:32:57 +0100

manuel (1.8.0-3) unstable; urgency=medium

  * deb/copyright: updated.
  * Added docs-no-updated-timestamp.diff (closes: #788941).
  * Added DEP-8 test suite.

 -- Daniel Stender <debian@danielstender.com>  Mon, 29 Jun 2015 07:52:38 +0200

manuel (1.8.0-2) unstable; urgency=low

  * Added Zope license for bootstrap.py to debian/copyright.
  * Added myself to uploaders.
  * Still initial release (Closes: #776871).

 -- W. Martin Borgert <debacle@debian.org>  Sat, 13 Jun 2015 22:37:40 +0000

manuel (1.8.0-1) unstable; urgency=medium

  * Initial release (Closes: #776871).

 -- Daniel Stender <debian@danielstender.com>  Wed, 29 Apr 2015 10:09:04 +0200
